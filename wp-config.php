<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpressuser');

/** MySQL database password */
define('DB_PASSWORD', 'password');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ny hvLWg2>&5`J$#,rLm<:Rl8{K(bOWBw$C_r#qFzJ|UdX7iX9kq=C4D3[:gr0#{');
define('SECURE_AUTH_KEY',  'Ev-[|(~]vdpb>GUyfdz*1tNLtt7-w$7k~)Ro waf-baDqA+.c@t3Iv^-C*f(0te*');
define('LOGGED_IN_KEY',    'qMJo=OMUB2XB!9nj$ZVlMisb:Y%>nkZ=a3/3+US5`X#=MpR4sreMaBrz-4kQ< jU');
define('NONCE_KEY',        '7wwl:iR.~$ZQgWhFO8R`?9)l:3,baKj_v0`Hn_erA(6M:k,+t>*^dil^G#@i}=!3');
define('AUTH_SALT',        'ON_K2)XxNK@WB?SG f8]zgjG>o9vlUuiPZ439.Z;FJIP+_ROWuIR2SqHyV,jz,7x');
define('SECURE_AUTH_SALT', 'Pm-awJzmWe5dHV)_y]~-2|iK{)8bg;ULP++T_?)~8|a&F!3)T#&,_]?bEM[%1/Hn');
define('LOGGED_IN_SALT',   'qDUJz@^n`TD>v>*[!v+A&*;f<|NgFK^dde)p[03O,N::_-D0n:!|wKE[9L/a!%<J');
define('NONCE_SALT',       ',1k7=qqctc0>h{;xSC,lakP+]5ht1H6VgV|?Rj9T?-R4$7E}EluyoCgfOjQ.SSHv');


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
